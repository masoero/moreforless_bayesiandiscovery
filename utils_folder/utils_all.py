import os
import numpy as np
from scipy.special import gammaln as gln




def log_poch(x,n):
    '''
    Input :
        x : <float>
        n : <int>
    Output :
        log_poch<float> log of Pochammer symbol of x of order n, i.e. ln(Gamma(x+n)/Gamma(x)) 
    Further details:
        http://mathworld.wolfram.com/PochhammerSymbol.html
    '''
    return gln(x+n) - gln(x)

def create_folder(path):
    '''
    Input :
        path : <str>
    Output :
        if there is not folder in path, such folder is created
    '''
    
    if not os.path.exists(path):
        os.makedirs(path)
        
def generate_bin_matrix_from_freqs(thetas, N, seed = 0):
    '''
        Input:
            thetas <array : K> # features
            N <int> # samples
            seed <int> random seed
        Output:
            X <array : N,K> binary matrix of features
    '''
    np.random.seed(seed)
    X = np.random.binomial(1, np.repeat(thetas, N)).reshape(len(thetas), N).T
    
    return X

def generate_cts_from_bin_mat(X):
    '''
        Input:
            X <array : N, K>
        Output:
            cts <array : N+1> at position n counts the number of distinct features observed at least once in the first n samples
    '''
    
    return np.concatenate([[0], np.count_nonzero(X.cumsum(axis=0), axis = 1)])