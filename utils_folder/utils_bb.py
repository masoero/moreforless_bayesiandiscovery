import scipy as sp
import numpy as np
from utils_all import *

from scipy import optimize
from scipy.special import beta as spbe
from scipy.special import binom as spbi
from scipy import stats as spst

def draw_power_law(low, high, exp, unif_samples):
    '''
    Generates random samples from a Zipf distribution in the interval [low, high];
    see http://mathworld.wolfram.com/RandomNumber.html for more details;
    Input:
        low float >0
        high float > low
        exp float > 0
        unif_samples array of len K; should be all uniform random numbers in [0,1]
    Output
        K power law distributed random variates
    '''
    return ((high**(exp + 1) - low**(exp+1))*unif_samples + low**(exp+1))**(1/(exp + 1))


def param_single_beta_binomial(M, N, alpha, beta):
    '''
    Input :
        M <int> this is the number of additional samples;
        N<int> this is the number of samples you've seen so far
        alpha, beta <float >0, float >0 > parameters
    Output :
        param_single : float >= 0, expected number of additional variants observed from step n+m-1 to step n+m
    '''
    return (N+beta-1)/N * (1-np.exp(log_poch(N+beta, M) - log_poch(N+alpha+beta, M)))

def param_array_bb(M, N, sfs_1_, optimal_params):
    '''
    Input :
        M < int > number of additional samples collected
        N < int > this is number of samples seen so far; should always be one of the elements of checkpoints
        sfs_1_ < int >
        alpha, beta <float >0, float >0 > parameters
    Output :
        param_array : array of size M. every entry m gives E[K_{N+m} - K_{N+m-1}]; 
    '''      
    alpha, beta = optimal_params
    return sfs_1_/alpha *  np.apply_along_axis(param_single_beta_binomial, 0, np.arange(1,M+1), N = N, alpha = alpha, beta = beta)

def compute_predicted_bb(M, N, sfs_1, optimal_params, train_counts):
    '''
    Input :
        M <int> this is the number of additional samples;
        N <int> this is the number of samples you've seen so far        
        sfs_1 : # variants observed exactly once
        train_counts : array of counts
    Output :
        total : <array> len(n+m) ;  total coincides with train_counts in coordinates 0:N
    '''
    total = np.zeros(N+M+1)
    total[:N+1] = train_counts[:N+1]
    total[N+1:] = param_array_bb(M, N, sfs_1, optimal_params) + train_counts[N]          
    return total

def optimize_parallel_bb_cost(path_to_sfs, path_to_sfs_array, path_to_counts, M, num_its):
    
    '''
    Input :
        
    '''
    sfs = np.loadtxt(path_to_sfs)
    sfs_1 = sfs[0]
    N = len(sfs)
    train_counts = np.loadtxt(path_to_counts)[:N+1].astype(int)
    optimal_params, optimal_values = optimize_bb(sfs, train_counts, num_its)
    preds = compute_predicted_bb(M, N, sfs_1, optimal_params, train_counts)
    
    return optimal_params, optimal_values, preds

def optimize_parallel_bb_mom(path_to_sfs, path_to_counts, M):
    
    '''
    Input :
        
    '''
    sfs = np.loadtxt(path_to_sfs)
    sfs_1 = sfs[0]
    N = len(sfs)
    train_counts = np.loadtxt(path_to_counts)[:N+1].astype(int)
    optimal_params = optimize_bb_mom(sfs, train_counts)
    preds = compute_predicted_bb(M, N, sfs_1, optimal_params, train_counts)
    
    return optimal_params, True, preds

def optimize_parallel_bb(path_to_sfs, path_to_sfs_1_array, path_to_counts, M, num_its):
    
    '''
    Input :
        
    '''
    sfs = np.loadtxt(path_to_sfs)
    sfs_1_array = np.loadtxt(path_to_sfs_1_array)
    sfs_1 = sfs[0]
    N = len(sfs)
    train_counts = np.loadtxt(path_to_counts)[:N+1].astype(int)
    optimal_params, optimal_values = optimize_bb_cost(sfs, sfs_1_array, train_counts, num_its)
    preds = compute_predicted_bb(M, N, sfs_1, optimal_params, train_counts)
    
    return optimal_params, optimal_values, preds


def optimize_bb(sfs, num_its, status):
    '''
    Input :
        path_to_counts <str>
        path_to_sfs : <str>  where is sfs
        num_its : number of times to perform numerical opt
    Output :
        optimal_params <array> (boot_its * num_its * 3)
        optimal_values <array> (boot_its * num_its)
        
    '''
    optimal_values_, optimal_params_ = np.zeros(num_its), np.zeros([num_its, 2])
    N = len(sfs)
    largest_entry = max(np.where(sfs>0)[0])
    sfs_pos = sfs[:largest_entry+1]
    log_like = make_ionita_like(N = N, sfs = sfs_pos)
    bnds = ((10e-100, 100), (10e-100, 100),)
    if status ==  True:
        for it in tqdm_notebook(range(num_its)):
                devol = optimize.differential_evolution(log_like, bnds)
                optimal_params_[it] = devol.x
                optimal_values_[it] = devol.fun
    else:
        for it in range(num_its):
                devol = optimize.differential_evolution(log_like, bnds)
                optimal_params_[it] = devol.x
                optimal_values_[it] = devol.fun        
    opt_ind = np.argmin(optimal_values_)        
    return optimal_params_[opt_ind], optimal_values_[opt_ind]


def optimize_bb_cost(sfs, sfs_1_array, train_counts, num_its):
    '''
    Input :
        path_to_counts <str>
        path_to_sfs : <str>  where is sfs
        num_its : number of times to perform numerical opt
    Output :
        optimal_params <array> (boot_its * num_its * 3)
        optimal_values <array> (boot_its * num_its)
        
    '''
    bnds = ((10e-100, 100), (10e-100, 100),)
    optimal_values_, optimal_params_ = np.zeros(num_its), np.zeros([num_its, 2])
    n = len(sfs)
    from_, up_to = int(n*2/3), n
    cost_ = make_cost_function_bb(sfs_1_array, train_counts, from_, up_to)

    for it in tqdm(range(num_its)):
            devol = optimize.differential_evolution(cost_, bnds)
            optimal_params_[it] = devol.x
            optimal_values_[it] = devol.fun
    opt_ind = np.argmin(optimal_values_)        
    return optimal_params_[opt_ind], optimal_values_[opt_ind]

def optimize_bb__mom_(sfs):
    '''
    Input :
        path_to_counts <str>
        path_to_sfs : <str>  where is sfs
        num_its : number of times to perform numerical opt
    Output :
        optimal_params <array> (boot_its * num_its * 3)
        optimal_values <array> (boot_its * num_its)
        
    '''
    bnds = ((10e-100, 100), (10e-100, 100),)
    N = len(sfs)
    return method_of_moments(N, sfs)


def make_ionita_like(N, sfs): 
    '''
    Input :
        n < integer >  number of samples
        sfs < array of ints > # this is only sfs up to last  positive  entry 
    Output :
        ionita_like  <fn>
            Input : alpha, beta <floats  > 0>
            Output log_like of (n,sfs) under beta model for parmas  alpha, beta 
                -- last un-numbered Eqn before Section 2 in 
                https://www.pnas.org/content/pnas/106/13/5008.full.pdf
    '''
    
    def ionita_like(params):
        '''
        Input :
            params = ((alpha, beta)) floats > 0
        Output :
            log_like
        '''
        
        alpha, beta = params
#         px = spbi(n, np.arange(1,n+1)) * spbe(np.arange(1,n+1) + alpha, n - np.arange(1,n+1) + beta) / spbe(alpha, beta)
        px = spbi(N, np.arange(1,N+1)) * np.exp(log_poch(alpha, np.arange(1,N+1)) + log_poch(beta, np.arange(1,N+1)[::-1]) -  log_poch(alpha+beta, N))

        px = px/px.sum()
        return - np.dot(sfs, np.log(px)[:len(sfs)])
    
    return ionita_like


def method_of_moments(sfs):
    N = len(sfs)
    k = int(sfs.sum())
    mean = 1/(n*k) * np.dot(np.arange(1,N+1), sfs)
    var = 1/(n*k**2) * np.dot(sfs, (np.arange(1,N+1) - mean)**2)
    alpha = mean * (mean*(1-mean)/var -1)
    beta = (1-mean)*(mean*(1-mean)/var - 1)

    return alpha, beta

    
def log_beta(alpha, beta):
    
    return gln(alpha)+gln(beta)-gln(alpha+beta)

def make_log_like(samples):
    N = len(samples)
    def log_like(params):
        alpha, beta = params
        return - (alpha-1) * np.log(samples).sum() - (beta-1)*np.log(1-samples).sum() + N * log_beta(alpha,beta)
    return log_like


def make_cost_function_bb(sfs_1, train_counts, from_, up_to):
    '''
    Input : 
        sfs_1 is a list of length up_to -  from_; each entry counts # things seen once at that step
        train_counts < array of ints > has len given by len(checkpoints); at each entry l, tells k_{checkpoints[l]}
        checkpoints < array of ints > sample sizes n at which we count k_n
        from_ < int in 0,1,...,len(checkpoints) > index of lowest count k_from_ to which we match the predictions
        up_to < int in from_+1,...,len(checkpoints) > index n_{low} of highest count k_up_to to which we match the predictions
    '''
    def cost_function(params):
        cost = 0
        for n in range(from_, up_to-1):
            predicted = compute_predicted_bb(up_to - n, n, sfs_1[n], params, train_counts)
            delta = predicted[N+1:up_to] - train_counts[N+1:up_to]
            cost += np.abs(delta).mean()
        return cost
    return cost_function


'''
	OPTIMAL DESIGN
'''

def optimal_design_bb(path_to_sfs, path_to_sfs_array, path_to_cts, n, dict_options):
    '''
        Input:
            path_to_data<str> refers to relvant data to look into
            n<int> number of training datapoints
	    dict_options<dict>
        
        Output:
            expected_news <array (num_its * grid_resolution*m_max+n)
            where each entry expected_news[it, g, l] is the expected number of variants to be observed at step n+l in
            the sampling process in bootstrap iteration it, for the g-th feasible couple (lambda_, m)
        estimates params = (alpha, c, sigma) computes E # new variants as a function of  the seq depth and breadth
        finds the combination (m, lambda_) which maximizes the number of variants discovered given the budget constraint 
        
    '''
    T, budget, num_its, p_err, grid_resolution = dict_options['T'], dict_options['budget'], dict_options['num_its'], dict_options['p_err'], dict_options['grid_resolution']
    bnds = ((10e-100, 100), (10e-100, 100),)
    train_counts = np.loadtxt(path_to_cts).astype(int)[:n+1]
    sfs = np.loadtxt(path_to_sfs).astype(int)
    sfs_1_array = np.loadtxt(path_to_sfs_array).astype(int)
    optimal_params, optimal_values = optimize_bb__cost_(sfs, sfs_1_array, train_counts, num_its)
    lambda_ls_, m_ls = feasible_points(budget, .9*T/2, 10*T, grid_resolution)
    max_news = int(1+n+max(m_ls))
    K_max = compute_support_size(optimal_params, n, sfs)
    expected_news = np.zeros([grid_resolution, max_news], dtype = int)
    expected_news[:, :n+1] = train_counts[np.newaxis]
    
    for g_ in tqdm(range(grid_resolution)):

        lambda_, m = lambda_ls_[g_], int(m_ls[g_])
        expected_news[g_, : n+m+1] = optimal_design_compute_predicted_bb__(m, n, K_max, optimal_params, train_counts, lambda_, T, p_err)
        expected_news[g_, n+m+1:] = expected_news[g_,n+m]*np.ones([max_news - (n+m+1)])
            
    results = {}
    
    results['expected_news'], results['lambda_ls'], results['m_ls'] = expected_news, lambda_ls_, m_ls
    results['n'], results['budget'], results['T']  = n, budget, T
    
    return results


def optimal_design_compute_predicted_bb__(m, n, K_max, optimal_params, train_counts, lambda_, T, p_err):
    '''
    Input :
        K_max < estimated or true number of total features, int >
        n, m < sample size and extrapolation size, ints >
        f_1, f_2 < frequencies in (0,1)>
        lambda_ < sequencing depth >
        p_err < error probability >
        T <threshold>
        train_counts < array of increasing counts, size >= n >
        alpha, beta <floats >0, beta params >
    Output :
        preds : <array of len n+m> 
    '''
    alpha, beta = optimal_params
    phi = 1 - spst.poisson.cdf(mu = lambda_*(1-p_err), k=T-1)
    preds = np.zeros(n+m+1)
    preds[:n+1] = train_counts[:n+1]
    rem = K_max * sp.integrate.quad(integrand, 0, 1, args = (n, phi, alpha, beta))[0]
    for m_ in range(n+1, n+m+1):
        preds[m_] = K_max * sp.integrate.quad(integrand, 0, 1, args = (m_, phi, alpha, beta))[0] - rem + train_counts[n]    
    return preds

def compute_support_size(params, n, fingerprints):
    '''
    Input:
        alpha, beta : floats > 0
        N < integer >  number of samples
        fingerprints < array of ints > 
    Output :
        S : cfr Delta(t), t->infty in un-numbered Eqn Section 1 in https://www.pnas.org/content/pnas/106/13/5008.full.pdf
    '''     
    alpha, beta = params
    return fingerprints.sum() + (fingerprints[0]/alpha) * (n+beta-1)/n

def integrand(x, n, phi, alpha, beta):
    '''
    Input:
        x <float in (0,1) >
        N < integer >
        phi < float in (0,1) >
        alpha, beta <floats >0 >
    Output :
        float -- integrand in Eqn (2) https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2942028/pdf/sagmb1581.pdf
    '''
    return (1 -np.exp(n*np.log(1-phi*x)))*np.exp((alpha-1)*np.log(x)+(beta-1)*np.log(1-x))/sp.special.beta(alpha, beta)

def generate_bin_matrix(alpha, beta, K_max, N):
    
    thetas = np.random.beta(alpha, beta, size = K_max)
    thetas = thetas[thetas>0]
    X = np.random.binomial(1, np.repeat(thetas, N).reshape(N, len(thetas)))
    
    return X, thetas

def generate_bin_matrix_from_freqs(thetas, N):
    
    X = np.random.binomial(1, np.repeat(thetas, N).reshape(N, len(thetas)))
    
    return X


