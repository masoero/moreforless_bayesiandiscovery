from utils_all import *
from scipy import optimize
from scipy import special as spspecial
from scipy import stats as spst
import numpy as np
import multiprocessing as mp



'''
    In what follows we use the notation from "More for Less: Predicting and Maximizing Genetic Variants via Bayesian Nonparametrics [Masoero et al., 2019+]"
    Specifically, we use the following notation:
    
        M, m : <int> denotes total extrapolation size, running index for exrapolation
        N_tot, N, n : <int> total sample size of available data, training size, running index for training
        r : <int> frequency of counts
        alpha, c, sigma : <float >0, float > - sigma, float in (0,1]> parameters of Three-Beta process (mass, concentration, discount)
        
'''

'''
    DATA GENERATING PROCESS
'''

def parameter_new(alpha, c, sigma,n):
    '''
    Input:
        alpha, c, sigma:parameters of 3 BP
        n <int> 
    Output:
        expected number of  new variants that n-th sample is displaying
    '''
    assert alpha>0 and c>-sigma and sigma>=0 and sigma <1
    return alpha * np.exp(log_poch(c+sigma, n-1) - log_poch(c+1, n-1))

def param_old(features_mat, c, sigma,n,k):
    '''
    Input:
        features mat: binary array 
        c, sigma: parameters of 3BP
        n <int>
        k <int>
    Output:
        probability that k-th feature is displayed by n-th observation given X_{1:n-1}
        
    '''
    assert c>-sigma and sigma>=0 and sigma <1
    return (np.sum(features_mat[:,k]) - sigma)/(c+n-1.0)

def three_parameters_IBP(alpha, c, sigma, N, print_status):
    assert alpha>0 and c>-sigma and sigma>=0 and sigma <1
    if print_status != False:
        st = time.time()
    new_k, K_cum, pn_v = np.zeros(N), np.zeros(N), np.zeros(N)
    for n in range(N):
        pn = parameter_new(alpha,c,sigma,n+1)
        new_k[n] = np.random.poisson(pn)
        K_cum[n] = int(np.sum(new_k))
        pn_v[n] = pn
    K_tot = int(K_cum[-1])
    features_mat = np.zeros([N, K_tot])
    
    for k in range(int(new_k[0])):
        features_mat[0,k] = 1
    for n in tqdm(range(1,N)):
        for k in range(int(K_cum[n-1])):
            xi = param_old(features_mat,c,sigma,n+1,k)
            features_mat[n,k] = np.random.binomial(1,xi)
        for k in range(int(K_cum[n-1]), int(K_cum[n])):
            features_mat[n,k] = 1
    if print_status != False:
        print(time.time()-st)
    return features_mat, new_k, K_cum

def seen_with_freq(x,f):
    cts = np.zeros(x.shape[0]+1)
    x_cum = x.cumsum(axis = 0)
    x_ = x.cumsum(axis = 0).astype(int)
    sfs = [np.bincount(x_[i]) for i in range(x_.shape[0])]
    
    for i in range(x.shape[0]):
        if len(sfs[i])<f+1:
            cts[i] = 0
        else:
            cts[i] = sfs[i][f]
    return cts

from matplotlib import ticker
def my_formatter_fun(x, p):
    return "%.0f" % (x * (10 ** scale_pow))
scale_pow = -1

def my_formatter_fun1(x, p):
    return "%.0f" % (x * (10 ** scale_pow1))
scale_pow1 = -2

def my_formatter_fun2(x, p):
    return "%.0f" % (x * (10 ** scale_pow2))
scale_pow2 = -3

def my_formatter_fun_x(x, p):
    return "%.0f" % (x * (10 ** scale_pow_x))
scale_pow_x = -2


'''
    PREDICTION
'''

def param_single(m, N, c, sigma):
    '''
    Output :
        param_single : float >= 0, expected number of additional variants observed from step n+m-1 to step n+m
    '''
    return np.exp(gln(c + sigma + N + m -1) - gln(c+ sigma) + gln(c + 1) - gln(c + N + m))


def param_array(M, N, optimal_params):
    '''
    Input :
        M < int > number of additional samples collected
        N < int > samples seen so far; 
        optimal_params: tuple
            alpha <float >0 > mass parameter
            c <float > - sigma> concentation parameter
            sigma <float in [0,1) > discount parameter
    Output :
        param_array : array of size M. every entry l gives E[J_{N+m} - J_{N+m-1}]; 
    '''        
    alpha, c , sigma = optimal_params
    return alpha * np.apply_along_axis(param_single, 0, np.arange(1,M+1), N = N, c = c, sigma = sigma)

def compute_predicted(M, N, optimal_params, train_counts):
    '''
    Input :
        M <int> this is the number of additional samples;
        N <int> this is the number of samples you've seen so far        
        optimal_params <array> (boot_its * 3)
        path_to_counts <array of ints>
    Output :
        preds : <array> len(N+M) ;  
            preds[N+m] : predicted number of variants seen up to step N+m; 
            preds[n] coincides with train_counts[n] for n <= N
    '''
    N = min(N, len(train_counts)-1)
    news = param_array(M = M, N = N, optimal_params = optimal_params).cumsum() # has length M
    preds = np.zeros(N+M+1)
    preds[:N+1] = train_counts[:N+1]
    preds[N+1:] = news + train_counts[N]          
    return preds




def param_single_freq(M, N, r, c, sigma):
    '''
    Input:
        M <int> this is the number of additional samples;
        N <int> this is the number of samples you've seen so far   
        r <int> this is the frequency of the observed variant
        c <float > - sigma> concentation parameter
        sigma <float in [0,1) > discount parameter
    Output :
        param_single_freq : float >= 0, expected number of additional variants observed from step N+M-1 to step N+M
    '''
    return spspecial.binom(M, r) * np.exp(log_poch(1-sigma, r-1) + log_poch(c+sigma, N+M-r) - log_poch(c+1, N+M-1))

def param_array_freq(M, N, r, optimal_params):
    '''
    Input :
        M < int > number of additional samples collected
        N < int > this is number of samples seen so far; should always be one of the elements of checkpoints
        r <int> things that will be observed f times
        optimal_params: tuple
            alpha <float >0 > mass parameter
            c <float > - sigma> concentation parameter
            sigma <float in [0,1) > tail parameter
    Output :
        param_array : array of size M. every entry l gives E[J_{N+m}^{(r)} - J_{N+m-1}^{(r)}]; 
    '''        
    alpha, c , sigma = optimal_params
    return alpha * np.apply_along_axis(param_single_freq, 0, np.arange(1,M+1), N = N, r = r, c = c, sigma = sigma)

def compute_predicted_freq(M, N, r, optimal_params, train_counts):
    '''
    Input :
        M <int> this is the number of additional samples;
        N <int> this is the number of samples you've seen so far      
        r <int> things that will be observed f times
        optimal_params <array> (boot_its * 3)
        train_counts <array of ints> here you should input things that have been observed r times up to step n for every n
    Output :
        preds : <array> len(N+M) ;  
            preds[N+m] : predicted number of variants appearing r times in additional samples; 
            preds[n] coincides with train_counts[n] for n <= N
    '''
    N = min(N, len(train_counts)-1)
    news = param_array_freq(M = M, N = N, r = r, optimal_params = optimal_params)
    preds = np.zeros(N+M+1)
    preds[:N+1] = train_counts[:N+1]
    preds[N+1:] = news + train_counts[N]          
    return preds

def make_cost_function(train_counts, from_, up_to, norm):
    '''
    Input : 
        train_counts < array of ints, len N > true distinct counts 
        from_ < int 0<= from < N > index of lowest sample from which we count J_{n_{low}} with our predictions
        up_to < int ; from_ < up_to <= N> index of highest sample to which we match the count J_{n_{hi}} with our predictions
        norm = int -- norm to be used
    Output :
        cost_function <function>; this is Eqn (***); cost function from using norm on true_cts with n_lo = from_ and n_hi = up_to
            Input : params alpha, c, sigma
            Output : scalar loss
    '''
    
    def cost_function(params):
        '''
            Takes as input parameters and returns discrepancy of true counts and predicted counts;
        '''
        predicted = compute_predicted(up_to - from_, from_, params, train_counts[:up_to])
        delta = predicted[from_:up_to] - train_counts[from_:up_to]
        cost = np.linalg.norm(delta, ord = norm)
        return cost
        
    return cost_function


def make_cost_function_cv(train_counts, from_, up_to, norm):
    '''
    Notice: this function is not described in detail in the paper; 
            it is a slight modification of Eqn (***) in which we shuffle k_cv times training data and 
            retain counts of distinct variants for every cv_k;
    Input : 
        train_counts < array of ints, shape = (k_cv * N_tot) > true distinct counts 
        from_ < int 0<= from < N > index of lowest sample from which we count J_{n_{low}} with our predictions
        up_to < int ; from_ < up_to <= N> index of highest sample to which we match the count J_{n_{hi}} with our predictions
        norm = int -- norm to be used
    Output :
        cost_function <function>; this is Eqn (***); cost function from using norm on true_cts with n_lo = from_ and n_hi = up_to
            Input : params alpha, c, sigma
            Output : scalar loss
    '''
    k = len(train_counts)
    
    def cost_function(params):
        predicted = np.asarray([compute_predicted(up_to - from_, from_, params, train_counts[j, :up_to]) for j in range(k)])
        delta = predicted[:, from_:up_to] - train_counts[:, from_:up_to]
        cost = np.linalg.norm(delta, ord = norm, axis = 1)
        return cost.sum()
        
    return cost_function


def make_log_efpf(sfs):
    '''
    Input :
        sfs <array of ints> notice that this needs to have length N; namely N is deduced from len(sfs); 
    Output :
        log_efpf <function>; this is the log of the exchangeable feature probability function 
        (e.g., Eqn (8) in https://arxiv.org/pdf/1301.6647.pdf) 
    '''
    N, J = len(sfs), sfs.sum()
    def log_efpf(params):
        '''
        Input :
            alpha, c, sigma
        '''
        sfs_ = sfs[sfs>0]
        alpha, c, sigma = params
        log_efpf_ = J * (np.log(alpha) - log_poch(c+1, N-1))
        log_efpf_ -= alpha * np.exp(log_poch(c+sigma, np.arange(1,N)) - log_poch(c+1, np.arange(1,N))).sum()
        log_efpf_ += np.dot(sfs_, log_poch(1-sigma, sfs_ - 1) + log_poch(c+sigma, N - sfs_))
        return - log_efpf_
    return log_efpf

def optimize_(train_counts, num_its, norm, status):
    '''
    Input :
        train_counts < array of ints ; len N > 
        num_its < int > number of times to optimization is performed
        norm < int > loss chosen
        status <bool> print status
    Output :
        optimal_params <array> (boot_its * num_its * 3)
        optimal_values <array> (boot_its * num_its)
    '''
    
    bnds =  ((10e-9,10e4), (0,10), (.0001, .9999),) # fixed bounds of support of 3-param beta process;
    optimal_values_, optimal_params_ = np.zeros(num_its), np.zeros([num_its,3])
    N = len(train_counts)
    from_, up_to = int(N * 2 / 3), N # n_lo and n_hi ; different choices could be done as long as 0<=n_lo<n_hi<=N
    cost = make_cost_function(train_counts, from_, up_to, norm)
    if status == True:
        for it in tqdm_notebook(range(num_its)):
            devol = optimize.differential_evolution(cost, bnds)
            optimal_params_[it] = devol.x
            optimal_values_[it] = devol.fun
    else:
        for it in range(num_its):
            devol = optimize.differential_evolution(cost, bnds)
            optimal_params_[it] = devol.x
            optimal_values_[it] = devol.fun
    opt_ind = np.argmin(optimal_values_)
    return optimal_params_[opt_ind], optimal_values_[opt_ind]


def optimize_cv(train_counts, num_its, norm):
    '''
    Input :
        train_counts < array of ints ; len N > 
        num_its < int > number of times to optimization is performed
        norm < int > loss chosen
    Output :
        optimal_params <array> (boot_its * num_its * 3)
        optimal_values <array> (boot_its * num_its)
        
    '''
    bnds =  ((10e-9,10e4), (0,10), (.0001, .9999),)
    optimal_values_, optimal_params_ = np.zeros(num_its), np.zeros([num_its,3])
    N = train_counts.shape[1]
    from_, up_to = int(N * 2 / 3), N
    cost = make_cost_function_cv(train_counts, from_, up_to, norm)
    for it in tqdm_notebook(range(num_its)):
        devol = optimize.differential_evolution(cost, bnds)
        optimal_params_[it] = devol.x
        optimal_values_[it] = devol.fun
    opt_ind = np.argmin(optimal_values_)
    return optimal_params_[opt_ind], optimal_values_[opt_ind]


def optimize_efpf(sfs, num_its):
    '''
    Input :
        path_to_counts
        num_its
    Output :
        optimal_params <array> (boot_its * num_its * 3)
        optimal_values <array> (boot_its * num_its)
        
    '''
    bnds =  ((10e-9,10e4), (0,10), (.0001, .9999),)
    optimal_values_, optimal_params_ = np.zeros(num_its), np.zeros([num_its,3])
    N = len(sfs)
    from_, up_to = int(N * 2 / 3), N
    cost = make_log_efpf(sfs)
    for it in tqdm_notebook(range(num_its)):
        devol = optimize.differential_evolution(cost, bnds)
        optimal_params_[it] = devol.x
        optimal_values_[it] = devol.fun
    opt_ind = np.argmin(optimal_values_)
    return optimal_params_[opt_ind], optimal_values_[opt_ind]

def predict_parallel(path_to_train_counts, M, N, num_its, norm):
    
    '''
    Input :
        path_to_train_counts <str>
        M <int> extrapolation size
        N < int > sample size
        num_its <int>
        norm <int> loss chosen
    Output :
        optimal_params (tuple of size 3)
        optimal_values (float)
        preds <array> of size N+M
    '''
    train_counts = np.loadtxt(path_to_counts)[:N+1].astype(int)
    optimal_params, optimal_values = optimize(train_counts, num_its, norm)
    preds = compute_predicted(M, N, optimal_params, train_counts)
    return optimal_params, optimal_values, preds


def predict_parallel_cv(path_to_train_counts, M, N, num_its, norm):
    
    '''
    Input :
        path_to_train_counts <str>
        M <int> extrapolation size
        N < int > sample size
        num_its <int>
        norm <int> loss chosen
        
    Output :
        optimal_params (tuple of size 3)
        optimal_values (float)
        preds <array> of size N+M
    '''
    train_counts = np.loadtxt(path_to_counts).astype(int)
    optimal_params, optimal_values = optimize_cv(train_counts, num_its, norm)
    preds = compute_predicted(M, N, optimal_params, train_counts[0])
    
    return optimal_params, optimal_values, preds



def optimize_parallel_efpf(path_to_train_counts, path_to_sfs, M, N, num_its):
    
    '''
    Input :
        path_to_train_counts <str>
        path_to_sfs <str>
        M <int> extrapolation size
        N < int > sample size
        num_its <int>
        norm <int> loss chosen
        
    Output :
        optimal_params (tuple of size 3)
        optimal_values (float)
        preds <array> of size n+m
    '''
    train_counts = np.loadtxt(path_to_counts)[:N+1].astype(int)
    sfs = np.loadtxt(path_to_sfs)
    optimal_params, optimal_values = optimize_efpf(sfs, num_its)
    preds = compute_predicted(M, N, optimal_params, train_counts)
    
    return optimal_params, optimal_values, preds


'''
    OPTIMAL DESIGN


'''

def feasible_points(budget, lowest_sequencing_depth, highest_sequencing_depth, grid_resolution):
    
    '''
    Input:
        budget : scalar > 0
        lowest_sequencing_depth <float >0> = lambda_min : lowest seq. depth for which we will evaluate objective function
        highest_sequencing depth <float > lowest_sequencing_depth> = lambda_max : highest seq. depth for which we will evaluate objective function
        grid_resolution <int> number of feasible couples (lambda, m) for which we are going to evaluate the objective
    Output :
        lambda_ls, m_ls two arrays of len grid_resolution; give collection of feasible (lambda, m)
    NB: we here assume Cost(m, lambda) = m * log(lambda)
    '''
    lambda_ls = np.linspace(lowest_sequencing_depth, highest_sequencing_depth, grid_resolution)
    m_ls =budget/np.log(lambda_ls)
    return lambda_ls, m_ls

def make_dict_options_(T, budget,  p_err, grid_resolution, lambda_init):
    '''
    Input:
        T <int >0> threshold of variant calling rule
        p_err <float in (0,1)> technology sequencing error
        grid_resolution <int > 0> number of couples for which we perform evaluation of objective
        lambda_init <float >0> sequencing depth of initial study
    '''
    dict_options = {}
    dict_options['T'], dict_options['budget'],  dict_options['p_err'], dict_options['grid_resolution'], dict_options['lambda_init'] = T, budget,  p_err, grid_resolution, lambda_init 
    
    return dict_options


def optimal_design_with_res(path_to_res, path_to_cts, dict_options):
    '''
        Input:
            paths_to_res <str> path to dict of bnp res; this contain
            path_to_cts <str>
            dict_options <dict> should  be created from created using appropriate function 
        
        Output:
            expected_news <array (num_its * grid_resolution*m_max+n)
            where each entry expected_news[it, g, l] is the expected number of variants to be observed at step n+l in
            the sampling process in bootstrap iteration it, for the g-th feasible couple (lambda_, m)
        estimates params = (alpha, c, sigma) computes E # new variants as a function of  the seq depth and breadth
        finds the combination (m, lambda_) which maximizes the number of variants discovered given the budget constraint 
        
    '''
    T, budget, p_err, grid_resolution = dict_options['T'], dict_options['budget'], dict_options['p_err'], dict_options['grid_resolution']
    lambda_init = dict_options['lambda_init']
    bnds = ((10e-9,10e4), (0,10), (.0001, .9999),)
    
    results = np.load(path_to_res).item()
    opt, N = results['opt_p'], results['N']
    num_boots = len(opt)
    lambda_ls_, M_ls = feasible_points(budget, .95 * T, 3*T, grid_resolution)
    max_news = int(1+N+max(M_ls))
    
    train_counts = np.loadtxt(path_to_cts).astype(int)
    expected_news = np.zeros([num_boots, grid_resolution, max_news], dtype = int)
    expected_news[:, :, :N+1] = train_counts[np.newaxis, np.newaxis, :N+1]
    for it in tqdm(range(num_boots)):
        optimal_params = opt[it]
        for g_ in range(grid_resolution):
            
            lambda_follow, m = lambda_ls_[g_], int(M_ls[g_])
            expected_news[it, g_, : N+M+1] = optimal_design_compute_predicted(M, N, optimal_params, train_counts, lambda_init, lambda_follow, T, p_err)
            expected_news[it, g_, N+M+1:] = expected_news[it, g_, N+M]*np.ones([max_news - (N+M+1)])

    results_opt = {}
    results_opt['expected_news'], results_opt['lambda_init'], results_opt['lambda_ls'], results_opt['M_ls'] = expected_news, lambda_init, lambda_ls_, M_ls
    results_opt['N'], results_opt['budget'], results_opt['T']  = N, budget, T
    results_opt['p_err'], results_opt['grid_resolution'] = p_err, grid_resolution
    results_opt['population'] = results['population']
    
    return results_opt

def optimal_design(path_to_cts, N, dict_options, norm):
    '''
        Input:
            path_to_cts <str> refers to relvant data to look into
            N <int> number of training datapoints
            dict_options <dict>
            norm <int>
        
        Output:
            expected_news <array (num_its * grid_resolution*max(M_ls)+N)
            where each entry expected_news[it, g, m] is the expected number of variants to be observed at step N+m in
            the sampling process in bootstrap iteration it, for the g-th feasible couple (lambda_, M_)
        estimates params = (alpha, c, sigma) computes E # new variants as a function of  the seq depth and breadth
        finds the combination (M, lambda_) which maximizes the number of variants discovered given the budget constraint 
        
    '''
    T, budget, num_its, p_err, grid_resolution, lambda_init = dict_options['T'], dict_options['budget'], dict_options['num_its'], dict_options['p_err'], dict_options['grid_resolution'], dict_options['lambda_init']
    bnds = ((10e-9,10e4), (0,10), (.0001, .9999),)
    train_counts = np.loadtxt(path_to_cts).astype(int)[:N+1]
    optimal_params, optimal_values = optimize(train_counts, num_its, norm)    
    lambda_ls_, M_ls = feasible_points(budget, .9*T/2, 3*T, grid_resolution)
    max_news = int(1+N+max(M_ls))
    expected_news = np.zeros([grid_resolution, max_news], dtype = int)
    expected_news[:, :N+1] = train_counts[np.newaxis]

    for g_ in tqdm(range(grid_resolution)):
        
        lambda_follow, M = lambda_ls_[g_], int(M_ls[g_])
        expected_news[g_, : N+M+1] = optimal_design_compute_predicted(M, N, optimal_params, train_counts, lambda_init, lambda_follow, T, p_err)
        expected_news[g_, N+M+1:] = expected_news[g_, N+M]*np.ones([max_news - (N+M+1)])
            
    results = {}
    
    results['expected_news'], results['lambda_init'], results['lambda_ls'], results['M_ls'] = expected_news, lambda_init, lambda_ls_, M_ls
    results['N'], results['budget'], results['T']  = N, budget, T
    
    return results

def optimal_design_compute_predicted(M, N, optimal_params, train_counts, lambda_init, lambda_follow, T, p_err):
    '''
    Input :
        M <int> this is the number of additional samples;
        N <int> this is the number of samples you've seen so far        
        optimal_params <array> (boot_its * 3)
        train_counts <array of len N> array of counts used for training
        lambda_init, lambda_follow : scalars > 0 seqn depth in initial and follow up studies
        T, p_err : threshold and error probabilities
    Output :
        total : <array> len(n+m) ;  total coincides with train_counts in coordinates 0:from_
    '''
    N = min(N, len(train_counts)-1)
    news = expected_new_with_error(M, N, optimal_params, lambda_init, lambda_follow, T, p_err)
    total = np.zeros(N+M+1)
    total[:N+1] = train_counts[:N+1]
    total[N+1:] = news + train_counts[N]          
    return total



def expected_new_with_error(M, N, optimal_params, lambda_init, lambda_follow, T, p_err):
    '''
    Input :
        M <int> this is the number of additional samples;
        N <int> this is the number of samples you've seen so far 
        lambda_init <float >0 > depth of sampling in initial study
        lambda_follow <float >0 > depth of sampling in subsequent study
        T <int >0 > truncation threshold
        p_err <float in (0,1) > sampling error probability
    '''
    alpha, c, sigma = optimal_params
    n_s = 100000
    phi_init = 1 - spst.poisson.cdf(mu = lambda_init * (1-p_err), k = T-1)
    phi_follow = 1 - spst.poisson.cdf(mu = lambda_follow * (1-p_err), k = T-1)
    betas = np.random.beta(a = 1-sigma, b = c+sigma, size = n_s)
    betas_ = pow(1-phi_init * betas, N) * pow(1-phi_follow * betas, np.arange(M)[:, np.newaxis])
    means = betas_.mean(axis = 1).cumsum()
    return alpha * phi_follow * means
